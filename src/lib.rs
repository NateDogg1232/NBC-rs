pub mod command;
pub mod opcode;

use crate::command::Command;

pub trait Ram {
    fn set_ram(&mut self, index: u32, value: u32);
    fn get_ram(&self, index: u32) -> u32;
    fn push_stack(&mut self, value: u32);
    fn pop_stack(&mut self) -> Option<u32>;
    fn push_proc_stack(&mut self, value: u32);
    fn pop_proc_stack(&mut self) -> Option<u32>;
}

pub trait Ports {
    fn port(&mut self, port_no: u32, command_no: u32, data: u32);
}

#[derive(Debug)]
pub enum CmpFlag {
    Equal,
    GreaterThan,
    LessThan,
}

pub trait NbcState {
    fn set_pc(&mut self, pc: u32);
    fn get_pc(&self) -> u32;
    fn get_cmp_flag(&self) -> CmpFlag;
    fn set_cmp_flag(&self, flag: CmpFlag);
}

pub type NbcResult = Result<(), NbcWarnError>;

#[derive(Debug)]
pub enum NbcWarnError {
    Warn(NbcWarning),
    Err(NbcError)
}

#[derive(Debug)]
pub enum NbcError {
    InvalidArgument(usize),
    UnknownOpcode(u16),
}

impl core::fmt::Display for NbcError {
    fn fmt(&self, f: &mut core::fmt::Formatter<'_>) -> core::fmt::Result {
        match self {
            NbcError::InvalidArgument(argno) => write!(f, "Invalid argument no {}", argno),
            NbcError::UnknownOpcode(op) => write!(f, "Encountered unknown opcode {:X}", op),
        }
    }
}

#[derive(Debug)]
pub enum NbcWarning {
    NopEncountered,
    EndOfProgram,
    HaltEncountered,
}

impl core::fmt::Display for NbcWarning {
    fn fmt(&self, f: &mut core::fmt::Formatter<'_>) -> core::fmt::Result {
        match self {
            NbcWarning::NopEncountered => write!(f, "Interpreter encountered a Nop opcode"),
            NbcWarning::EndOfProgram => write!(f, "End of program reached"),
            NbcWarning::HaltEncountered => write!(f, "Halt opcode encountered"),
        }
    }
}

pub type Program = [u32];

fn get_command_args<T: Ram + NbcState>(command: &mut Command, machine: &T, program: &Program) -> NbcResult {
    let pc = machine.get_pc();
    for (i, arg) in command.args.iter_mut().enumerate() {
        if arg.is_none() {
            // Ignore any blank arguments
            continue;
        }
        let mut arg = arg.unwrap();
        // Temporarily bring in the ArgFlag value for neat code
        use crate::command::ArgFlag;
        let prog_arg_value = program[(pc as usize) + i + 1];
        arg.value = Some(match arg.flag {
            ArgFlag::Constant => prog_arg_value,
            ArgFlag::Address => machine.get_ram(prog_arg_value),
            ArgFlag::Indirect => machine.get_ram(machine.get_ram(prog_arg_value)),
            _ => return Err(NbcWarnError::Err(NbcError::InvalidArgument(i))),
        });
    }
    Ok(())
}

pub fn do_tick<T: Ram + Ports + NbcState>(machine: &mut T, program: &Program) -> NbcResult {
    let mut command = Command::from(program[machine.get_pc() as usize]);
    get_command_args(&mut command, machine, program)?;

    match command.op {
        opcode::Opcode::Nop => {
            // We'll keep the Nop implementation here since it's super simple
            end_op(&command, machine)?;
        },
        opcode::Opcode::Halt => {
            return Err(NbcWarnError::Warn(NbcWarning::HaltEncountered));
        },
        opcode::Opcode::Port => {
            do_op_port(&command, machine)?;
            end_op(&command, machine)?;
        },
        opcode::Opcode::Mov => {
            do_op_mov(&command, machine)?;
            end_op(&command, machine)?;
        },
        opcode::Opcode::Add => {
            do_op_add(&command, machine)?;
            end_op(&command, machine)?;
        }
        opcode::Opcode::Unknown(o) => {
            return Err(NbcWarnError::Err(NbcError::UnknownOpcode(o)));
        }
        o => {
            unimplemented!("Opcode {:?} is not yet implemented", o);
        }
    }

    Ok(())
}

// End an opcode
fn end_op<T: NbcState>(command: &Command, machine: &mut T) -> NbcResult {
    let mut pc = machine.get_pc();
    pc += opcode::Opcode::get_no_args(command.op) as u32;
    pc += 1;
    machine.set_pc(pc);
    Ok(())
}

// ----------------------
// Opcode implementations
// ----------------------

fn do_op_port<T: NbcState + Ports>(command: &Command, machine: &mut T) -> NbcResult {
    machine.port(command.args[0].unwrap().value.unwrap(), command.args[1].unwrap().value.unwrap(), command.args[2].unwrap().value.unwrap());
    Ok(())
}

fn do_op_mov<T: NbcState + Ram>(command: &Command, machine: &mut T) -> NbcResult {
    machine.set_ram(command.args[1].unwrap().value.unwrap(), command.args[0].unwrap().value.unwrap());
    Ok(())
}

fn do_op_add<T: NbcState + Ram>(command: &Command, machine: &mut T) -> NbcResult {
    let result = command.args[0].unwrap().value.unwrap() + command.args[0].unwrap().value.unwrap();
    machine.set_ram(command.args[3].unwrap().value.unwrap(), result);
    Ok(())
}

fn do_op_sub<T: NbcState + Ram>(command: &Command, machine: &mut T) -> NbcResult {
    let result = command.args[0].unwrap().value.unwrap() - command.args[0].unwrap().value.unwrap();
    machine.set_ram(command.args[3].unwrap().value.unwrap(), result);
    Ok(())
}

fn do_op_mul<T: NbcState + Ram>(command: &Command, machine: &mut T) -> NbcResult {
    let result = command.args[0].unwrap().value.unwrap() - command.args[0].unwrap().value.unwrap();
    machine.set_ram(command.args[3].unwrap().value.unwrap(), result);
    Ok(())
}

fn do_op_div<T: NbcState + Ram>(command: &Command, machine: &mut T) -> NbcResult {
    let result = command.args[0].unwrap().value.unwrap() - command.args[0].unwrap().value.unwrap();
    machine.set_ram(command.args[3].unwrap().value.unwrap(), result);
    Ok(())
}

fn do_op_mod<T: NbcState + Ram>(command: &Command, machine: &mut T) -> NbcResult {
    let result = command.args[0].unwrap().value.unwrap() - command.args[0].unwrap().value.unwrap();
    machine.set_ram(command.args[3].unwrap().value.unwrap(), result);
    Ok(())
}

fn do_op_not<T: NbcState + Ram>(command: &Command, machine: &mut T) -> NbcResult {
    let result = !command.args[0].unwrap().value.unwrap();
    machine.set_ram(command.args[3].unwrap().value.unwrap(), result);
    Ok(())
}


