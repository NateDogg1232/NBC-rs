#[derive(Debug, Clone, Copy, Eq, PartialEq)]
pub enum Opcode {
    //System. 0x0---
    Nop,
    Halt,
    Port,
    Mov,
    // Math: 0x1---
    Add,
    Sub,
    Mul,
    Div,
    Mod,
    Not,
    Or,
    And,
    Xor,
    // Compare: 0x2---
    Cmp,
    // Branching: 0x3---
    Jmp,
    Jeq,
    Jne,
    Jg,
    Jl,
    Jsr,
    Ret,
    // Stack: 0x4---
    Push,
    Pop,

    // Used internally by this implementation
    Unknown(u16),
}

impl Opcode {
    pub const fn get_no_args(op: Self) -> usize {
        match op {
            Opcode::Nop => 0,
            Opcode::Halt => 0,
            Opcode::Port => 3,
            Opcode::Mov => 2,

            Opcode::Add
            | Opcode::Sub
            | Opcode::Mul
            | Opcode::Div
            | Opcode::Mod
            | Opcode::Or
            | Opcode::And
            | Opcode::Xor => 3,

            Opcode::Not => 2,

            Opcode::Cmp => 2,

            Opcode::Jmp | Opcode::Jeq | Opcode::Jne | Opcode::Jg | Opcode::Jl | Opcode::Jsr => 1,
            Opcode::Ret => 0,

            Opcode::Push => 1,
            Opcode::Pop => 1,

            _ => 0,
        }
    }
}

impl From<u16> for Opcode {
    fn from(int: u16) -> Opcode {
        match int {
            0x0000 => Opcode::Nop,
            0x0001 => Opcode::Halt,
            0x0002 => Opcode::Port,
            0x0003 => Opcode::Mov,

            0x1000 => Opcode::Add,
            0x1001 => Opcode::Sub,
            0x1002 => Opcode::Mul,
            0x1003 => Opcode::Div,
            0x1004 => Opcode::Mod,
            0x1005 => Opcode::Not,
            0x1006 => Opcode::Or,
            0x1007 => Opcode::And,
            0x1008 => Opcode::Xor,

            0x2000 => Opcode::Cmp,

            0x3000 => Opcode::Jmp,
            0x3001 => Opcode::Jeq,
            0x3002 => Opcode::Jne,
            0x3003 => Opcode::Jg,
            0x3004 => Opcode::Jl,
            0x3005 => Opcode::Jsr,
            0x3006 => Opcode::Ret,

            0x4000 => Opcode::Push,
            0x4001 => Opcode::Pop,

            o => Opcode::Unknown(o),
        }
    }
}

#[cfg(test)]
mod tests {
    use super::Opcode;
    #[test]
    fn parse_op_nop() {
        assert_eq!(Opcode::from(0x0000), Opcode::Nop);
    }
    #[test]
    fn parse_op_halt() {
        assert_eq!(Opcode::from(0x0001), Opcode::Halt);
    }
    #[test]
    fn parse_op_port() {
        assert_eq!(Opcode::from(0x0002), Opcode::Port);
    }
    #[test]
    fn parse_op_mov() {
        assert_eq!(Opcode::from(0x0003), Opcode::Mov);
    }
    #[test]
    fn parse_op_add() {
        assert_eq!(Opcode::from(0x1000), Opcode::Add);
    }
    #[test]
    fn parse_op_sub() {
        assert_eq!(Opcode::from(0x1001), Opcode::Sub);
    }
    #[test]
    fn parse_op_mul() {
        assert_eq!(Opcode::from(0x1002), Opcode::Mul);
    }
    #[test]
    fn parse_op_div() {
        assert_eq!(Opcode::from(0x1003), Opcode::Div);
    }
    #[test]
    fn parse_op_mod() {
        assert_eq!(Opcode::from(0x1004), Opcode::Mod);
    }
    #[test]
    fn parse_op_not() {
        assert_eq!(Opcode::from(0x1005), Opcode::Not);
    }
    #[test]
    fn parse_op_or() {
        assert_eq!(Opcode::from(0x1006), Opcode::Or);
    }
    #[test]
    fn parse_op_and() {
        assert_eq!(Opcode::from(0x1007), Opcode::And);
    }
    #[test]
    fn parse_op_xor() {
        assert_eq!(Opcode::from(0x1008), Opcode::Xor);
    }
    #[test]
    fn parse_op_cmp() {
        assert_eq!(Opcode::from(0x2000), Opcode::Cmp);
    }
    #[test]
    fn parse_op_jmp() {
        assert_eq!(Opcode::from(0x3000), Opcode::Jmp);
    }
    #[test]
    fn parse_op_jeq() {
        assert_eq!(Opcode::from(0x3001), Opcode::Jeq);
    }
    #[test]
    fn parse_op_jne() {
        assert_eq!(Opcode::from(0x3002), Opcode::Jne);
    }
    #[test]
    fn parse_op_jg() {
        assert_eq!(Opcode::from(0x3003), Opcode::Jg);
    }
    #[test]
    fn parse_op_jl() {
        assert_eq!(Opcode::from(0x3004), Opcode::Jl);
    }
    #[test]
    fn parse_op_jsr() {
        assert_eq!(Opcode::from(0x3005), Opcode::Jsr);
    }
    #[test]
    fn parse_op_ret() {
        assert_eq!(Opcode::from(0x3006), Opcode::Ret);
    }
    #[test]
    fn parse_op_push() {
        assert_eq!(Opcode::from(0x4000), Opcode::Push);
    }
    #[test]
    fn parse_op_pop() {
        assert_eq!(Opcode::from(0x4001), Opcode::Pop);
    }

    // Checking the argument count
    #[test]
    fn num_args_op_nop() {
        assert_eq!(Opcode::get_no_args(Opcode::Nop), 0);
    }
    #[test]
    fn num_args_op_port() {
        assert_eq!(Opcode::get_no_args(Opcode::Port), 3);
    }
    #[test]
    fn num_args_op_mov() {
        assert_eq!(Opcode::get_no_args(Opcode::Mov), 2);
    }
    #[test]
    fn num_args_op_add() {
        assert_eq!(Opcode::get_no_args(Opcode::Add), 3);
    }
}
