use super::opcode::Opcode;
#[derive(Debug, Clone, Copy)]
pub struct Command {
    pub op: Opcode,
    pub args: [Option<Argument>; 4],
}

#[derive(Debug, Clone, Copy, Eq, PartialEq)]
pub struct Argument {
    pub flag: ArgFlag,
    pub value: Option<u32>,
}

#[derive(Debug, Clone, Copy, Eq, PartialEq)]
#[repr(u8)]
pub enum ArgFlag {
    Constant = 0,
    Address = 1,
    Indirect = 2,
    Invalid = 3,
}

impl From<u8> for ArgFlag {
    fn from(int: u8) -> Self {
        let int = int & 0x3; // (%0x11)
        match int {
            0 => Self::Constant,
            1 => Self::Address,
            2 => Self::Indirect,
            _ => Self::Invalid,
        }
    }
}

impl From<u16> for ArgFlag {
    fn from(int: u16) -> Self {
        Self::from(int as u8)
    }
}

impl Command {
    pub fn new() -> Self {
        Self {
            op: Opcode::Unknown(0),
            args: [None; 4],
        }
    }
    pub fn set_opcode(mut self, cmd: u32) -> Self {
        self.op = Opcode::from((cmd & 0xFFFF) as u16);
        self
    }
    pub fn set_arg_flags(mut self, cmd: u32) -> Self {
        // Get the argument flags
        let hi_cmd = ((cmd & 0xFFFF0000) >> 16) as u16;
        for i in 0..Opcode::get_no_args(self.op) {
            self.args[i] = Some(Argument {
                flag: ArgFlag::from(hi_cmd >> (i * 2)),
                value: None,
            })
        }
        self
    }
}

impl From<u32> for Command {
    fn from(int: u32) -> Self {
        Self::new().set_opcode(int).set_arg_flags(int)
    }
}
