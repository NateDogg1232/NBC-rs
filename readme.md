# NBC

The worst bytecode you will ever see

---
## Basic instruction architecture

The minimum size for an opcode is 32 bits. The max size is 32 * 4. The size depends on how many arguments the opcode takes.  
RAM is 32 bits wide (Meaning all numbers are u32)


### Opcode structure

- b0-b15: Opcode
- b16-17:arg1 flags
- b18-19:arg2 flags
- b20-21:arg3 flags
- b22-31:Unused (Must be set to 0)

#### Argument flag structure

Each argument had a flag in the opcode specifying what type of argument the instruction is following. Each flag is a number from 0 to 3

0. Constant
1. Address
2. Indirect addressing
3. Unused (Program will panic if using this)

### Endianness
Big endian

---
## Opcodes

- System `0x0---`
  - `0x0000: NOP`
    - Does nothing
  - `0x0001: HALT`
    - `Ends the program`
  - `0x0002: PORT`
    - Gives port command
      - Arg1: Port
      - Arg2: Command
      - Arg3: Data
  - `0x0003: MOV`
    - Sets a RAM location's value
      - Arg1: Value
      - Arg2: Destination
- Math `0x1---`
  - Structure of all math operations:
    - Arg1: Operand 1
    - Arg2: Operand 2
    - Arg3: Destination
  - `0x1000: ADD`
    - Adds two operands and stores them
  - `0x1001: SUB`
    - Subtracts two operands and stores them
  - `0x1002: MUL`
    - Multiplies two operands and stores them
  - `0x1003: DIV`
    - Divides two operands and stores them
  - `0x1004: MOD`
    - Divides two operands and stores the remainder
  - `0x1005: NOT`
    - Bitwise NOTs one operand and stores it
  - `0x1006: OR`
    - Bitwise ORs two operands and stores them
  - `0x1007: AND`
    - Bitwise ANDs two operands and stores them
  - `0x1008: XOR`
    - Bitwise XORs two operands and stores them
- Compare `0x2---`
  - `0x2000: CMP`
    - Compares two operands and sets a flag stating its compared value
- Branching `0x3---`
  - `0x3000: JMP`
    - Jumps to a place in the program
  - `0x3001: JEQ`
    - JMPs if CMP returned equal
  - `0x3002: JNE`
    - JMPs if CMP returned not equal
  - `0x3003: JG`
    - JMPs if CMP returned greater than
  - `0x3004: JL`
    - JMPs if CMP returned less than
  - `0x3005: JSR`
    - JMPs with the ability to return, pushing a value to a seprate stack called the proc stack
  - `0x3006: RET`
    - Returns from a JSR by popping the last value pushed to the proc stack
- Stack manipulation `0x4---`
  - `0x4000: PUSH`
    - Pushes one value to the stack
  - `0x4001: POP`
    - Pops a value off of the stack to the address specified
  
### Stack Operations

The stack is FILO (First In Last Out) meaning
the first value you push will be the last one
popped off of it

For example:

```
PUSH #01
PUSH #02
POP $00 'Returns 2
POP $01 'Returns 1
```

---
## Ports

Main IO is through ports

Each port takes a command value and a data
value. This may change from architecture to architecture, but the ones stated are the ones which are required

- Port 0: Standard Commands
  - Command 0: Get available RAM
    - Data: Where to store the return value
  - Command 1: Get character from buffer
    - Data: Where to store the current key
  - Command 2: Wait for keystroke then get character
    - Data: Where to store the key pressed
  - Command 3: Output character in a TTY fashion
- Port 1: Unstandard Commands
  - Command 0: Set Color

